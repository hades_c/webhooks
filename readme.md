Webhooks-git.oschina.net
=================================================================
专为git.oschina.net制作的git钩子服务端。<br><br>
服务器需要安装nodejs<br><br>
然后把这个项目布置到服务器上。<br><br>
npm install<br><br>
node app.js<br><br>
推荐使用 forever 来后台启动进程.<br><br>

##app设置 <br><br>
//自定义参数<br><br>
var password = '123123123'; //提交密码<br><br>
var releasewords = 'release'; //发布关键字,只有包含关键字的message才会发布到环境中<br><br>


##.sh文件设置 <br><br>
deploy.sh中<br><br>
28行 cd /home/www/$PROJECT_NAME<br><br>
/home/www/请修改成自己的项目群目录<br><br>
会根据请示地址中p的参数,对这个目录下的参数项目进行git pull<br><br>


##PUSH钩子设置 <br><br>
钩子地址:IP/deploy/?p=项目名字.<br><br>
密码:自定义


##其它
提供了简单的提交日志功能
./access.log
